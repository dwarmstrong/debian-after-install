## Introduction

Script **debian-after-install.sh** configures a Debian GNU/Linux system.

It is ideally run after first boot into a [minimal install](https://www.dwarmstrong.org/minimal-debian/) of **Debian 12 aka "Bookworm"**.

Run `debian_after_install.sh -h` for options.

## Summary

* **SERVER** or **DESKTOP** configuration is available
* SERVER installs packages for a basic console setup, where DESKTOP installs the [Openbox](https://www.dwarmstrong.org/openbox/) window manager
* Customize GRUB
* Auto-install security updates (optional)
* [ZRAM for swap](https://www.dwarmstrong.org/zram-linuxmint/) (optional)
* Select different keyboard and console-font (optional)

## How does it work?

Sync package repositories and install `git`:

```bash
sudo apt update && sudo apt install git
```

Download:

```bash
git clone https://gitlab.com/dwarmstrong/debian-after-install.git
```

Change directory and run script:

```bash
cd debian-after-install
sudo ./debian_after_install.sh
```

## Author

[Daniel Wayne Armstrong](https://www.dwarmstrong.org)

## License

GPLv3. See [LICENSE](https://gitlab.com/dwarmstrong/debian-after-install/-/blob/main/LICENSE.md) for more details.
