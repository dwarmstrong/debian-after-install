#!/usr/bin/env bash
# Author: Daniel Wayne Armstrong <hello@dwarmstrong.org>

set -euo pipefail

SCRIPT=$(basename $0)
DISTRO="Debian"
VERSION="12"
CODENAME="bookworm"
PURPOSE="Setup device after a fresh install of ${DISTRO} GNU/Linux $VERSION "
PURPOSE+="aka \"${CODENAME}\"."

URL="https://www.dwarmstrong.org/minimal-debian/"
URL2="https://www.dwarmstrong.org/openbox/"

NAME="foo"
PROFILE="foobar"
AUTO_UPDATE="no"
KBD="no"
FONT="no"
ZRAM="no"

# ANSI escape codes
RED="\\033[1;31m"
GREEN="\\033[1;32m"
BLUE="\\033[1;34m"
YELLOW="\\033[1;33m"
PURPLE="\\033[1;35m"
NC="\\033[0m" # no colour


Help() {
  echo "usage: $SCRIPT [OPTION]"
  echo ""
  echo "$PURPOSE"
  echo ""
  echo "options:"
  echo "  -h    show this help message and exit"
  echo ""
}


run_options() {
  while getopts ":h" OPT; do
    case $OPT in
      h)  Help
          exit;;
      \?) echo "error: Invalid option"
          exit;;
    esac
  done
}


load_libraries () {
  local lib="lib_linux_setup.sh"
  local source="https://gitlab.com/dwarmstrong/homebin/-/blob/master/${lib}"
  local libhome="files/${lib}"

  # Add useful functions:
  if [[ -f "${libhome}" ]]; then
    source ${libhome}
  else
    echo "error: $libhome not found"
    echo "source: $source"
    exit 1
  fi
}


verify_version() {
  local version="VERSION_ID=\"${VERSION}\""
  local name="VERSION_CODENAME=${CODENAME}"
  local file="/etc/os-release"
  
  if ! [[ $(grep $version $file) ]] || ! [[ $(grep $name $file) ]]; then
    err "Script requires $DISTRO version $VERSION codenamed \"${CODENAME}\"."
    exit 1
  fi
}


verify_homedir() {
  if ! [[ -d "/home/${NAME}" ]]; then
    err "Home directory for $NAME not found."
    exit 1
  fi
}


greeting() {
  local msg="Script '${SCRIPT}' configures a ${DISTRO} GNU/Linux system."
  
  clear
  echo ""
  echo_yellow "(O< -- Hello! $msg"
  echo_yellow "(/)_"
  echo ""
  cat <<EOF
It is ideally run after first boot into a minimal install [1] of
$DISTRO $VERSION aka "$CODENAME".

A choice of either SERVER or DESKTOP configuration is available.
SERVER installs packages for a basic console setup [2], whereas
DESKTOP installs the Openbox [4] window manager.

See '${SCRIPT} -h' for options and the README for
more details.

EOF
  echo "[1] \"Minimal Debian Bookworm\""
  echo_blue "    <https://www.dwarmstrong.org/minimal-debian/>"
  echo "[2] \"New life for an old laptop as a Linux home server\""
  echo_blue "    <https://www.dwarmstrong.org/laptop-home-server/>"
  echo "[3] \"Roll your own Linux desktop using Openbox\""
  echo_blue "    <https://www.dwarmstrong.org/openbox/>"
  echo ""

  while : ; do
    read -r -n 1 -p "Proceed? [Yn] => " reply
    if [[ "${reply}" == [nN] ]]; then
      printf "\nExit.\n"
      exit 0
    elif [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      echo ""
      break
    else
      echo ""
    fi
  done
}


questionnaire() {
  local query="7"

  while : ; do
    echo_yellow "Question 1 of ${query}"
    echo "Enter name of the superuser."
    while : ; do
      read -r -p "=> " reply
      if [[ -z "${reply}" ]]; then
        :
      else
        NAME="${reply}"
        break
      fi
    done
    verify_homedir
    echo ""

    echo_yellow "Question 2 of ${query}"
    echo "Setup this computer as: [1] Server [2] Desktop (Openbox)"
    while : ; do
      read -r -n 1 -p "Choose a number => " reply
      if [[ -z "${reply}" ]]; then
        :
      elif [[ "${reply}" == 1 ]]; then
        PROFILE="server"
        break
      elif [[ "${reply}" == 2 ]]; then
        PROFILE="openbox"
        break
      else
        printf "\nInvalid reply.\n"
      fi
    done
    echo ""
    echo ""

    echo_yellow "Question 3 of ${query}"
    echo "Automatically install security fixes (unattended-upgrades(8))."
    while : ; do
      read -r -n 1 -p "Auto-install security updates? [Yn] => " reply
      if [[ "${reply}" == [nN] ]]; then
        AUTO_UPDATE="no"
        break
      elif [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
        AUTO_UPDATE="yes"
        break
      else
        printf "\nInvalid reply.\n"
      fi
    done
    echo ""
    echo ""

    echo_yellow "Question 4 of ${query}"
    echo "Change the model of keyboard and/or the keyboard map."
    while : ; do
      read -r -n 1 -p "Setup different keyboard configuration? [Yn] => " reply
      if [[ "${reply}" == [nN] ]]; then
        KBD="no"
        break
      elif [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
        KBD="yes"
        break
      else
        printf "\nInvalid reply.\n"
      fi
    done
    echo ""
    echo ""

    echo_yellow "Question 5 of ${query}"
    echo "Change the font and font-size used in the console."
    while : ; do
      read -r -n 1 -p "Setup a different console font? [Yn] => " reply
      if [[ "${reply}" == [nN] ]]; then
        FONT="no"
        break
      elif [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
        FONT="yes"
        break
      else
        printf "\nInvalid reply.\n"
      fi
    done
    echo ""
    echo ""

    echo_yellow "Question 6 of ${query}"
    echo "Create a swap device in RAM with the *zram* kernel module."
    while : ; do
      read -r -n 1 -p "Proceed? [Yn] => " reply
      if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
        ZRAM="yes"
        break
      elif [[ "${reply}" == [nN] ]]; then
        ZRAM="no"
        break
      else
        printf "\nInvalid reply.\n"
      fi
    done
    echo ""
    echo ""

    echo_yellow "Question ${query} of ${query}"
    echo_purple "User: $NAME"
    echo_purple "Profile: $PROFILE"
    if [[ "$AUTO_UPDATE" == "yes" ]]; then
      echo_green "Automatic Updates: $AUTO_UPDATE"
    else
      echo_red "Automatic Updates: $AUTO_UPDATE"
    fi
    if [[ "$KBD" == "yes" ]]; then
      echo_green "Configure Keyboard: $KBD"
    else
      echo_red "Configure Keyboard: $KBD"
    fi
    if [[ "$FONT" == "yes" ]]; then
      echo_green "Configure font: $FONT"
    else
      echo_red "Configure font: $FONT"
    fi
    if [[ "$ZRAM" == "yes" ]]; then
      echo_green "Zram: $ZRAM"
    else
      echo_red "Zram: $ZRAM"
    fi
    echo ""
    read -r -n 1 -p "Is this correct? [Yn] or q(uit) => " reply
    if [[ "${reply}" == [yY] || "${reply}" == "" ]]; then
      printf "\nOK....Let's roll in...."
      countdown 5
      break
    elif [[ "${reply}" == [nN] ]]; then
      printf "\nOK....Let's try again....\n\n\n"
    elif [[ "${reply}" == [qQ] ]]; then
      printf "\nExit.\n"
      exit 0
    else
      err "Invalid reply."
    fi
  done
}


configure_consolefont() {
  dpkg-reconfigure console-setup
}

configure_keyboard() {
  dpkg-reconfigure keyboard-configuration
  systemctl restart keyboard-setup.service
  update-initramfs -u -k all
}


configure_sources() {
  # Add repositories, update package list, upgrade packages:
  local sources="/etc/apt/sources.list"
  local mirror="http://deb.debian.org/debian/"
  local security_mirror="http://security.debian.org/debian-security"
  local repos="main contrib non-free non-free-firmware"

  backup_file $sources
  cat > $sources <<EOF
deb $mirror $CODENAME $repos
#deb-src $mirror $CODENAME $repos

deb $security_mirror ${CODENAME}-security $repos
#deb-src $security_mirror ${CODENAME}-security $repos

deb $mirror ${CODENAME}-updates $repos
#deb-src $mirror ${CODENAME}-updates $repos

deb $mirror ${CODENAME}-backports $repos
#deb-src $mirror ${CODENAME}-backports $repos
EOF
}


upgrade_system() {
  apt-get update
  apt-get -y dist-upgrade
}


console_packages() {
  local pyenv
  local pkgs
  pyenv="make build-essential libssl-dev zlib1g-dev libbz2-dev "
  pyenv+="libreadline-dev libsqlite3-dev wget curl llvm libncursesw5-dev "
  pyenv+="xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev"
  pkgs="${pyenv} acpi apt-file apt-show-versions apt-utils aptitude bsd-mailx "
  pkgs+="check-dfsg-status command-not-found cowsay cryptsetup curl figlet "
  pkgs+="firmware-misc-nonfree fuse3 git gnupg htop libfuse2 lm-sensors "
  pkgs+="lolcat lshw man-db ncal ncdu newsboat openssh-server plocate "
  pkgs+="python3-systemd rsync sl speedtest-cli sudo tmux tree unzip vim "
  pkgs+="whois wget"

  apt-get -y install $pkgs
  if [[ $(grep "AuthenticAMD" /proc/cpuinfo) ]]; then
    apt-get -y install amd64-microcode
  else
    apt-get -y install intel-microcode
  fi
  adduser $NAME mail
  apt-file update
}


syncthing_package() {
  local keyring="/etc/apt/keyrings/syncthing-archive-keyring.gpg"
  local repository="https://apt.syncthing.net/"
  local list="/etc/apt/sources.list.d/syncthing.list"
  local pin="Pin: origin apt.syncthing.net"
  local pref="/etc/apt/preferences.d/syncthing.pref"

  # Enable system to check package authenticity by adding the release PGP key:
  curl -L -o ${keyring} https://syncthing.net/release-key.gpg
  # Stable channel is updated with stable release builds, usually every first
  # Tuesday of the month. Add the "stable" channel to APT sources:
  echo "deb [signed-by=${keyring}] $repository syncthing stable" > $list
  # Ensure system packages do not take preference over those in the syncthing
  # repo by assigning packages in that repo a higher priority ("pinning"):
  printf "Package: *\n${pin}\nPin-Priority: 990\n" > $prefer
  # Install:
  apt update && apt install syncthing
}


boinc_package() {
  local keyring="/etc/apt/keyrings/boinc-archive-keyring.gpg"
  local keyserver="--keyserver keyserver.ubuntu.com"
  local key="--recv-keys 40254C9B29853EA6"
  local distro="$CODENAME $CODENAME main"
  local repository="https://boinc.berkeley.edu/dl/linux/stable/${distro}"
  local list="/etc/apt/sources.list.d/boinc.list"
  local pin="Pin: origin boinc.berkeley.edu"
  local prefer="/etc/apt/preferences.d/boinc.pref"

  gpg --homedir /tmp --no-default-keyring --keyring $keyring $keyserver $key
  echo "deb [arch=amd64 signed-by=${keyring}] $repository" > $list
  printf "Package: *\n${pin}\nPin-Priority: 990\n" > $prefer
  apt update
  #apt install boinc
}


server_packages() {
  local pkgs="logwatch powermgmt-base rdiff-backup vbetool"
  
  apt-get -y install $pkgs
}


xorg_packages() {
  local pkgs
  pkgs="xorg x11-utils xbacklight xbindkeys xvkbd xinit xinput "
  pkgs+="xserver-xorg-input-all xterm"

  apt-get -y install $pkgs
}


desktop_packages() {
  local pkgs
  pkgs="adwaita-qt alsa-utils feh ffmpeg firefox-esr fonts-dejavu "
  pkgs+="fonts-firacode fonts-font-awesome fonts-liberation2 fonts-ubuntu "
  pkgs+="gimp gimp-data-extras gimp-help-en gthumb gtk2-engines-murrine "
  pkgs+="gtk2-engines-pixbuf imagemagick imv keychain libglib2.0-bin "
  pkgs+="libgtk-3-0 libgtk-4-1 libnotify-bin lximage-qt mesa-vulkan-drivers "
  pkgs+="vulkan-tools network-manager network-manager-gnome "
  pkgs+="papirus-icon-theme pavucontrol pipewire-audio qpdfview "
  pkgs+="qt5-style-plugins qt5ct rhythmbox shellcheck thunderbird "
  pkgs+="transmission-gtk vlc"

  apt-get -y install $pkgs
}


openbox_packages() {
  local pkgs
  pkgs="openbox obconf dbus-x11 dunst hsetroot i3lock libxcb-xinerama0 "
  pkgs+="lxappearance menu picom rofi scrot tint2 volumeicon-alsa "
  pkgs+="xfce4-power-manager xfce4-terminal"

  xorg_packages
  desktop_packages
  apt-get -y install $pkgs
}


install_packages() {
  console_packages
  syncthing_package
  boinc_package
  if [[ "$PROFILE" == "server" ]]; then
    server_packages
  elif [[ "$PROFILE" == "openbox" ]]; then
    openbox_packages
  fi
  # ... train kept rolling ...
  /usr/games/sl | /usr/games/lolcat
}


zram_swap() {
  local conf
  conf="/etc/default/zramswap"

  apt-get -y install zram-tools
  swapoff --all
  zramswap stop
  backup_file $conf
  sed -i "s/^#ALGO.*/ALGO=zstd/" $conf
  sed -i "s/^#PERCENT.*/PERCENT=25/" $conf
  sed -i "s/^#PRIORITY.*/PRIORITY=100/" $conf
  backup_file /etc/fstab
  sed -e '/swap/ s/^#*/#/' -i /etc/fstab
  zramswap start
}


grub() {
  local conf="61_theme.cfg"
  local font="dejavu-sans-mono-20.pf2"
  local png="background.png"
  local custom="40_custom"

  if ! [[ -f "/boot/grub/${png}" ]]; then
    cp files/grub/${png} /boot/grub/${png}
  fi
  if ! [[ -f "/boot/grub/fonts/${font}" ]]; then
    cp files/grub/${font} /boot/grub/fonts/${font}
  fi
  if ! [[ -f "/etc/default/grub.d/${conf}" ]]; then
    cp files/grub/${conf} /etc/default/grub.d/${conf}
  fi
  cp files/grub/${custom} /etc/grub.d/${custom}
  update-grub
}


home_ssh() {
  local ssh_dir="/home/${NAME}/.ssh"
  local auth_key="${ssh_dir}/authorized_keys"

  if ! [[ -d "${ssh_dir}" ]]; then
    mkdir "${ssh_dir}"
    chmod 700 "${ssh_dir}"
    touch "${auth_key}"
    chmod 600 "${auth_key}"
    chown -R "${NAME}": "${ssh_dir}"
  fi
}


auto_update() {
  local file="50unattended-upgrades"

  apt-get -y install unattended-upgrades
  cp files/${file} /etc/apt/apt.conf.d/${file}
  dpkg-reconfigure -plow unattended-upgrades
}


configure_alternatives() {
  update-alternatives --config editor
  if [[ "${PROFILE}" == "openbox" ]]; then
    update-alternatives --config x-terminal-emulator
  fi
}


main() {
  verify_version
  verify_root
  banner "START"
  greeting
  banner "QUESTIONNAIRE"
  questionnaire
  if [[ "$FONT" == "yes" ]]; then
    banner "CONFIGURE CONSOLE FONT"
    configure_consolefont
  fi
  if [[ "$KBD" == "yes" ]]; then
    banner "CONFIGURE KEYBOARD"
    configure_keyboard
  fi
  banner "UPGRADE SYSTEM"
  configure_sources
  upgrade_system
  banner "INSTALL PACKAGES"
  install_packages
  if [[ "$ZRAM" == "yes" ]]; then
    banner "ENABLE ZRAM SWAP"
    zram_swap
  fi
  if [[ -d "/boot/grub" ]]; then
    banner "CUSTOMIZE GRUB"
    grub
  fi
  banner "SETUP ${NAME}"
  home_ssh
  if [[ "$AUTO_UPDATE" == "yes" ]]; then
    banner "ENABLE UNATTENDED UPGRADES"
    auto_update
  fi
  configure_alternatives
  banner "FINISH"
  au_revoir "Done! ${DISTRO} is in tip-top form and ready to reboot."
}


# (O<  Let's go!
# (/)_
run_options "$@"
load_libraries
main
